import * as React from "react";
import { AlbumSearchField, AlbumSearchResults } from "../containers/Search";

export class MusicSearch extends React.Component<{}> {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <AlbumSearchField />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <AlbumSearchResults />  
          </div>
        </div>
      </div>
    );
  }
}
